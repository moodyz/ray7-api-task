class TripsController < ApplicationController
	def index
		trips = Trip.get_trips(params[:user_id])
		render json: trips
	end

	def join_trip

		message = Trip.join(params[:trip_id],params[:user_id])
		render json: message , status: message[:status]
		
	end

	def joined_users
		users = Trip.find_by_id(params[:trip_id]).users
		render json: users 
	end

	def leave_trip

		message = Trip.leave(params[:trip_id],params[:user_id])
		render json: message , status: message[:status]
		
	end


	def create
		trip = Trip.create!(trip_params)
		render json: trip

	end 
	def delete
		message = Trip.delete_trip(params[:user_id],params[:trip_id])
		render json: message
	end


	private 
	def trip_params
		params.permit(:driver_id,:source_id,:destination_id,:departure_time,:seats)
	end
end

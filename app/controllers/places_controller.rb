class PlacesController < ApplicationController
	
	def index
		place = Place.all
		render :json => place
	end 

	def create
		place = Place.create!(place_params)
		render json: place
	end

	private

		def place_params
			params.permit(:name,:longitude,:latitude)
		end
end

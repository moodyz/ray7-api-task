class GroupsController < ApplicationController
	def index 
		#this function should return all the available groups
		groups = Group.all
		render json: groups
	end 

	def create
		#this function should create a group
		group = Group.create!(group_params)
		render json: group
	end 

	private 
	def group_params
		#this function is for making the params which are accepted more secure
		params.permit(:name)
	end
end

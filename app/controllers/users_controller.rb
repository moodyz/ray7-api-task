class UsersController < ApplicationController
	
	def create
		user = User.create!(user_params)
		render json: user
	end 

	private
	def user_params
		params.permit(:first_name,:last_name,:phone,:group_id,:work_place_id,:home_place_id)
	end 
end

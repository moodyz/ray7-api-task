class Place < ApplicationRecord
	has_many :user_home_places ,:class_name =>"User",:foreign_key =>"home_place_id" 
	has_many :user_work_places ,:class_name =>"User",:foreign_key =>"work_place_id"
	has_many :trip_source ,:class_name =>"Trip",:foreign_key =>"source_id" 
	has_many :trip_destination ,:class_name =>"Trip",:foreign_key =>"destination_id"

	validates :name ,presence: true ,uniqueness: true
	validates :longitude ,presence: true
	validates :latitude ,presence: true
end

class User < ApplicationRecord
	belongs_to :work_place , :class_name =>"Place"
	belongs_to :home_place , :class_name =>"Place"
	has_and_belongs_to_many :trips
	belongs_to :group
	has_many :owned_trips , :class_name =>"Trip", :foreign_key =>"driver_id"

	validates :first_name ,presence: true
	validates :last_name ,presence: true
	validates :phone ,presence: true

end

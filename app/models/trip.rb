class Trip < ApplicationRecord
	belongs_to :driver,:class_name =>"User"
	has_and_belongs_to_many :users
	before_destroy {|trip| trip.users.clear}
	belongs_to :source , :class_name =>"Place"
	belongs_to :destination, :class_name =>"Place"

	validates :seats , presence: true
	validates :departure_time , presence: true 
	

	 def self.get_trips(user_id)
		#get the trips that has the same source and destinations of the user's home and work
		#getting the user that want to see the trips he is able to join
		user = User.find_by_id(user_id)
		if(user !=nil)
		#getting the trips that has the same source as his home and destination as his work and also has empty seats and trips am not a driver in and the driver is in my same group
		trips = Trip.where(source_id:user.home_place.id,destination_id:user.work_place.id).map { |trip| ( trip.users.count<trip.seats && trip.driver_id !=user.id && trip.driver.group.id == user.group.id ? trip : nil) }.compact()
		else
			return {message:"please provide an existing user"}
		end

	end

	def self.join(trip_id,user_id)
		trip = Trip.find_by_id(trip_id)
		user = User.find_by_id(user_id)

		if trip.driver.group.id != user.group.id
			return {status:401,message:"you must be in the same group as the driver"}
		
		end
		if trip != nil and user != nil
			#checking of the user is already in the trip or not
			if !trip.users.where(id: user.id).blank?
				return {status:401,message:"you are already in this trip"}
			end
			#checking if there is seats available and the driver is not the requesting user
			if trip.users.count<trip.seats  && trip.driver_id !=user.id
				trip.users << user
				return {status:200,message:"user is added sucssesfully to the trip enjoy your ride"}
			else
				return {status:412,message:"faild to add user to the trip as it is full or you are a driver for this trip and already joined as a driver"}
			end
			
		else
			return {status:400,message:"make sure that the enterd user or trip are valid"}

		end

	end

	def self.leave(trip_id,user_id)
		trip = Trip.find_by_id(trip_id)
		user = User.find_by_id(user_id)
		#valid trip an valid user
		if trip != nil and user != nil
			#check if user is in the trip
			if trip.users.find_by_id(user_id)
				trip.users.destroy(user.id)
				return {status:200,message:"user is removed sucssesfully from the trip"}
			else
				return {status:412,message:"faild to remove user as he is not found in this trip"}
			end
			
		else
			return {status:400,message:"make sure that the enterd user or trip are valid"}

		end

	end


	def self.delete_trip(user_id ,trip_id)
		user = User.find_by_id(user_id)
		trip = Trip.find_by_id(trip_id)
		if user !=nil && trip != nil
			#valid trip an valid user
			if user.id == trip.driver_id
				#checking if the user is the trip owner or not
				Trip.destroy(trip_id)
				return {status:200,message:"trip is successfuly removed"}
			else
				return {status:401,message:"only a trip owner can remove a trip"}
			end 

		else

		return {status:400,message:"make sure that the enterd user or trip are valid"}

		end
	end
	
end

class CreatePlaces < ActiveRecord::Migration[5.1]
  def change
    create_table :places do |t|

      t.string :name ,:unique =>true
      t.decimal :longitude  
      t.decimal :latitude  
    end
  end
end

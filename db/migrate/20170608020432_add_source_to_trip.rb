class AddSourceToTrip < ActiveRecord::Migration[5.1]
  def change
    add_column :trips, :source_id, :integer
  end
end

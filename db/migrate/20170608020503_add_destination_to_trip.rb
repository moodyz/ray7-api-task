class AddDestinationToTrip < ActiveRecord::Migration[5.1]
  def change
    add_column :trips, :destination_id, :integer
  end
end

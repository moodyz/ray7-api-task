Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  	post '/user'=>"users#create"
  	post '/groups' =>"groups#create"
  	get '/groups' =>"groups#index"
  	post '/places' =>"places#create"
  	get '/places' =>"places#index"
  	get "/trips" =>"trips#index"
  	post '/trips' =>"trips#create"
  	post '/trips/:trip_id/join' =>"trips#join_trip"
  	post '/trips/:trip_id/leave' =>"trips#leave_trip"
  	delete '/trips/:trip_id' => "trips#delete"
  	get 'trips/:trip_id' =>"trips#joined_users"

end

FactoryGirl.define do
  factory :group do |group|
    group.name {Faker::Name.name}
  end
end
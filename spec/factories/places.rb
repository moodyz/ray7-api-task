FactoryGirl.define do
  factory :place do
    name { Faker::Name.name}
    longitude{Faker::Number.decimal(2,5)}
    latitude{Faker::Number.decimal(2,5)}
  end
end
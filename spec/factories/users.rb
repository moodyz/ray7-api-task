FactoryGirl.define do
  factory :user do
    first_name{Faker::Lorem.word}
    last_name {Faker::Lorem.word}
    phone{Faker::Number.number(11)}
    group_id nil
    work_place_id nil
    home_place_id nil
  end
end
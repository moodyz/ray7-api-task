FactoryGirl.define do
  factory :trip do
    driver_id nil 
    source_id nil
    destination_id nil
    departure_time {Faker::Time.between(DateTime.now - 1, DateTime.now)}
    seats {Faker::Number.number(1)}
  end
end
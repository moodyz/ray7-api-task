require 'rails_helper'

RSpec.describe GroupsController, type: :controller do

	#here we just initialize some of the data we are going to use
	let!(:groups_data){create_list(:group,10)}
	
	let (:group){{name: "all the way up"}} 

	#over here we define the tests of the functions we are using

	describe 'GET /groups' do

		#over here we do the call to the get group method in the controller
		before {get :index}
		#using before to call it before any checks...
		#and validate the returned json

		it 'returns the groups' do 
			#here we check if the json is successfully returned and it is not empty
			json = JSON.parse(response.body) #parsing the data that is returnd
			expect(json).not_to be_empty
			expect(json.size).to eq(10)
		end

		it 'returns a 200 OK code' do 
			expect(response).to have_http_status(200)
		end

	end

	describe 'POST /groups' do 
		
		before{post :create,params: group}

		it 'creates a group and return its json' do
			json = JSON.parse(response.body)
			expect(json['name']).to eq("all the way up")
		end
		it 'returns a 200 OK code' do 
			expect(response).to have_http_status(200)
		end

	end 


end

require 'rails_helper'

RSpec.describe UsersController, type: :controller do

	describe 'POST /user' do
		
			let (:group){create(:group)}
			let (:home_place){create(:place)}
			let (:work_place){create(:place)}
			let(:data){{first_name: "mohamed",
						last_name:"hussein",
						phone:"01141819680",
						group_id: group.id,
						home_place_id: home_place.id,
						work_place_id: work_place.id}}

			before{post :create,params: data}

			it 'creates a user' do 
				json = JSON.parse(response.body)
				expect(json['first_name']).to eq("mohamed")
				expect(json['last_name']).to eq("hussein")
				expect(json['phone']).to eq("01141819680")
			end

			it 'has a valid response code' do 
				expect(response).to have_http_status(200)
			end 
		end
end

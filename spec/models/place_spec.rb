require 'rails_helper'

RSpec.describe Place, type: :model do

  it{should validate_presence_of(:name)}
  it{should validate_uniqueness_of(:name)}
  it{should validate_presence_of(:longitude)}
  it{should validate_presence_of(:latitude)}
  it{should have_many(:user_home_places)}
  it{should have_many(:user_work_places)}
  it{should have_many(:trip_source)}
  it{should have_many(:trip_destination)}
end

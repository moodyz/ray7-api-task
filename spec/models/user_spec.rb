require 'rails_helper'

RSpec.describe User, type: :model do
  it {should validate_presence_of (:first_name)}
  it {should validate_presence_of (:last_name)}
  it {should validate_presence_of (:phone)}
  it {should belong_to(:group)}
  it {should belong_to(:work_place)}
  it {should belong_to(:home_place)}
  it {should have_and_belong_to_many(:trips)}
  it {should have_many(:owned_trips)}
end

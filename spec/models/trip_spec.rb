require 'rails_helper'

RSpec.describe Trip, type: :model do
  it {should belong_to (:driver)}
  it {should have_and_belong_to_many(:users)}
  it {should belong_to (:source)}
  it {should belong_to (:destination)}
  it {should validate_presence_of(:seats)}
  it {should validate_presence_of(:departure_time)}
 
end

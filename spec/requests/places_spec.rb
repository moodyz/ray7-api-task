require "rails_helper"
RSpec.describe 'Places API' ,type: :request  do
	
	#here we just initialize some of the data we are going to use
	let!(:places){create_list(:place,10)}
	
	let (:place){{name: "mokatam",longitude:"31.25689",latitude:"32.56894"}} 

	#over here we define the tests of the functions we are using

	describe 'GET /places' do

		#over here we do the call to the get place method in the controller
		before {get'/places'}
		#using before to call it before any checks...
		#and validate the returned json

		it 'returns the places' do 
			#here we check if the json is successfully returned and it is not empty
			json = JSON.parse(response.body) #parsing the data that is returnd
			expect(json).not_to be_empty
			expect(json.size).to eq(10)
		end


		it 'returns a 200 OK code' do 
			expect(response).to have_http_status(200)
		end

	end

	describe 'POST /places' do 
		
		before{post'/places',params: place}

		it 'creates a place and return its json' do
			json = JSON.parse(response.body)
			expect(json['name']).to eq("mokatam")
		end
		it 'returns a 200 OK code' do 
			expect(response).to have_http_status(200)
		end

	end 


end
require "rails_helper"
RSpec.describe "Trips API",type: :request do
	#data initialization for testing 
	let (:source){create(:place)}
	let (:destination){create(:place)}
	let (:group){create(:group)}
	let (:group2){create(:group)}
	let(:driver){create(:user,:work_place_id => destination.id , :home_place_id => source.id, :group_id => group.id)}
	let(:seats){Random.rand(1..3)}
	let(:trips_list){create_list(:trip,10,:source_id => source.id,:destination_id => destination.id,:driver_id => driver.id)}
	before{10.times{post '/trips', params: data}}
	let(:user){create(:user,:work_place_id => destination.id , :home_place_id => source.id, :group_id => group.id)}
	let(:user2){create(:user,:work_place_id => destination.id , :home_place_id => source.id, :group_id => group2.id)}
	#creating the trip example
		let(:data){{ "driver_id": driver.id, 
				"source_id": source.id,
				 "destination_id": destination.id, 
				 "departure_time": "10/6/2017", 
				 "seats": seats
				 }}
	describe 'POST /trips' do

			let(:data){{ "driver_id": driver.id, 
				"source_id": source.id,
				 "destination_id": destination.id, 
				 "departure_time": "10/6/2017", 
				 "seats": seats
				 }}
		#before running the test send a creation request
		before{post '/trips', params: data}

		it 'creates a trip with the sent data' do

			json = JSON.parse(response.body)

			expect(json['driver_id']).to eq(driver.id)
			expect(json['source_id']).to eq(source.id)
			expect(json['destination_id']).to eq(destination.id)
			expect(json['seats']).to eq(seats)
		end

		it 'has a valid response code' do 
				expect(response).to have_http_status(200)
		end 

	end

	describe 'get /trips' do 

		
		#creating 10 dummy trips

		#calls the method that returns the available trips
		before{get "/trips",:params => {:user_id=>user.id} }

		it 'returns the list of the available trips for this user'do 
		    json =JSON.parse(response.body)
		    expect(json.size).to eq(trips_list.size)

		end

		it 'has a valid response code' do 
				expect(response).to have_http_status(200)
		end 

	end


	describe 'post /trips/:trip_id/join success' do
		before{post "/trips/#{trips_list[0].id}/join",params: {user_id: user.id}}
		it 'user should join a trip ' do 
			json = JSON.parse(response.body)
			expect(json['message']).to eq("user is added sucssesfully to the trip enjoy your ride")
			expect(json['status']).to eq(200)
		end

		it 'has a valid response code' do 
				expect(response).to have_http_status(200)
		end

	end

	describe 'post /trips/:trip_id/join failure' do
		before{post "/trips/#{trips_list[0].id}/join",params: {user_id: user2.id}}
		it 'user should join a trip ' do 
			json = JSON.parse(response.body)
			expect(json['message']).to eq("you must be in the same group as the driver")
			expect(json['status']).to eq(401)
		end

		it 'has a valid response code' do 
				expect(response).to have_http_status(401)
		end

	end

	describe 'post /trips/:trip_id/join success' do
		before{post "/trips/#{trips_list[0].id}/join",params: {user_id: user.id}}
		before{post "/trips/#{trips_list[0].id}/leave",params: {user_id: user.id}}
		it 'user should leave a trip ' do 
			json = JSON.parse(response.body)
			expect(json['message']).to eq("user is removed sucssesfully from the trip")
			expect(json['status']).to eq(200)
		end

		it 'has a valid response code' do 
				expect(response).to have_http_status(200)
		end

	end


	describe 'post /trips/:trip_id/join failure' do
		before{post "/trips/#{trips_list[0].id}/join",params: {user_id: user.id}}
		before{post "/trips/#{trips_list[0].id}/leave",params: {user_id: user2.id}}
		it 'user should leave a trip ' do 
			json = JSON.parse(response.body)
			expect(json['message']).to eq("faild to remove user as he is not found in this trip")
			expect(json['status']).to eq(412)
		end

		it 'has a valid response code' do 
				expect(response).to have_http_status(412)
		end

	end

	describe 'post /trips/:trip_id/join trip not found' do
		before{post "/trips/#{-5}/leave",params: {user_id: user2.id}}
		it 'user should leave a trip ' do 
			json = JSON.parse(response.body)
			expect(json['message']).to eq("make sure that the enterd user or trip are valid")
			expect(json['status']).to eq(400)
		end

		it 'has a valid response code' do 
				expect(response).to have_http_status(400)
		end

	end


end